/**
 * Created by weekee on 31/3/16.
 */
//variable
var name = 'fred';
var age = 50;
var married = true;
var address;

console.info("name = " + name);
console.info("age = " + age);
console.info("married = " + married);
console.info("address = " + address);

/*
< less than
<= less than or equal
== equal
!= not equal
 */

if (age>50) {
    console.log("apply for medishield")
} else
    console.log("give seat to elders")

//initialising my vars
var i = 0;
document.write("<ul>");
while (i<5) {
    document.write("<li>Item " + i + "</li>");
    i++;
}
document.write("</ul>")

//initialising my vars = quicker way
document.write("<ul>")
for (i=0; i<5; i++) {
    document.write("<li>*** Item " + i + "</li>");
}
document.write("</ul>")
