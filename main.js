/**
 * Created by weekee on 31/3/16.
 */
//load express
var express = require("express");

//create an application
var app = express();

/*specific to server my document root
static resources will be served from here*/
app.use(
    express.static(__dirname + "/public"));

//instruct server to start listening to port 3000, and function instructs message to confirm that this worked.
app.listen(3000, function() {
    console.info("Application has started listening on port 3000")
});

